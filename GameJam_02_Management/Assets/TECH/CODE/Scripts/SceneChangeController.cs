﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
public class SceneChangeController : MonoBehaviour
{
   
    #region Singleton
    public static SceneChangeController s_Instance { get; private set; }

    public event Action restartScene;
    public event Action loadGame;

    private bool isRestartingScene = false;
    private bool isLoadingGame = false;
    private void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void RestartScene()
    {
        if (isRestartingScene) return;
        StartCoroutine(IERestartScene());
    }

    IEnumerator IERestartScene()
    {
        isRestartingScene = true;
        AsyncOperation assync = SceneManager.LoadSceneAsync(0);
        while (!assync.isDone)
        {
            yield return null;
        }
        restartScene?.Invoke();
        isRestartingScene = false;
    }

    public void LoadGame()
    {
        if (isLoadingGame) return;
        StartCoroutine(IELoadGame());
    }
    IEnumerator IELoadGame()
    {
        isLoadingGame = true;
        AsyncOperation assync = SceneManager.LoadSceneAsync(0);
        while (!assync.isDone)
        {
            yield return null;
        }
        loadGame?.Invoke();
        isLoadingGame = false;
    }
}
