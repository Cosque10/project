﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCollider : MonoBehaviour
{
    [SerializeField] private EnemyAI m_ParentEnemy = null;
    [SerializeField] private CopAnimations m_CopAnimations = null;

    private KarenAnimations _karenAnimations;
    private KarenSystems _karenSystems;

    private void OnEnable()
    {
        _karenAnimations = FindObjectOfType<KarenAnimations>();
        _karenSystems = FindObjectOfType<KarenSystems>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished) return;
        KarenSystems karenSystem = other.GetComponent<KarenSystems>();
        if (karenSystem == null || karenSystem.gotCaught) return;

        PlaySoundsController.s_Instance.PlayKarenGetCaught();
        karenSystem.gotCaught = true;
        m_ParentEnemy.GotCaught();
        _karenAnimations.PlayKarenBustedAnimation();
        _karenSystems.StopEtourdissement();

        m_CopAnimations.PlayArrestedAnimation();
        EndCanvasController.s_Instance.FadeLoseTopCop();
    }


}
