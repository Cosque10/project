﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPoint : MonoBehaviour
{
    [SerializeField] public int m_Index = 0;

    public bool isTaken { get; set; }

    private void Start()
    {
        isTaken = false;
    }
}
