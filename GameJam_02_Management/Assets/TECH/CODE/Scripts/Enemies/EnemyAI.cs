﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAI : MonoBehaviour
{
    [SerializeField] private NavMeshAgent m_Agent = null;
    [SerializeField] private float m_GetPatrolPointsDistance = 25.0f;
    [SerializeField] public int m_Index = 0;
    public List<PatrolPoint> _patrolPoints = new List<PatrolPoint>();

    public PatrolPoint _currentPatrolPoint { get; set; }


    public bool karenGotCaught = false;
 

    public void OnLoadGame()
    {
        SetNextDestination();
    }

    public void OnStartGame()
    {
        GetProximityPatrolPoints();
        _currentPatrolPoint = RandomPatrolPoint();
        SetNextDestination();
    }

    private void OnEnable()
    {
        GameManager.s_Instance.gamePaused += SetDestinationToNull;
        GameManager.s_Instance.gameUnpaused += SetNextDestination;
        GameManager.s_Instance.gameStarted += OnStartGame;
        SceneChangeController.s_Instance.loadGame += OnLoadGame;
    }
    private void OnDestroy()
    {
        GameManager.s_Instance.gamePaused -= SetDestinationToNull;
        GameManager.s_Instance.gameUnpaused -= SetNextDestination;
        GameManager.s_Instance.gameStarted -= OnStartGame;
        SceneChangeController.s_Instance.loadGame -= OnLoadGame;
    }
    private void Update()
    {
        if (!GameManager.s_Instance.isInGame() || karenGotCaught) return;
        MoveToNextPoint();
    }

    private void GetProximityPatrolPoints()
    {
        PatrolPoint[] patrolPoints = FindObjectsOfType<PatrolPoint>();

        foreach (PatrolPoint point in patrolPoints)
        {
            Vector3 direction = transform.position - point.transform.position;
            direction.y = 0.0f;
            if(direction.magnitude < m_GetPatrolPointsDistance)
            {
                _patrolPoints.Add(point);
            }
        }
    }

    private PatrolPoint RandomPatrolPoint()
    {
        int random = Random.Range(0, _patrolPoints.Count);
        PatrolPoint newPAtrolPoint = _patrolPoints[random];

        return newPAtrolPoint;
    }

    public void GotCaught()
    {
        karenGotCaught = true;
        SetDestinationToNull();
    }

    private void MoveToNextPoint()
    {
        if (HasArrivedToPoint())
        {
            _currentPatrolPoint = RandomPatrolPoint();
            SetNextDestination();
        }
    }

    private bool HasArrivedToPoint()
    {
        if (_currentPatrolPoint == null) return false;
        float distance = Vector3.Distance(transform.position,_currentPatrolPoint.transform.position);
        return distance < 1.0f;
    }

    public void SetNextDestination()
    {
        if (_currentPatrolPoint == null) return;
        Vector3 destination = _currentPatrolPoint.transform.position;
        destination.y = transform.position.y;
        m_Agent.SetDestination(destination);
    }

    public void SetDestinationToNull()
    {
        m_Agent.SetDestination(transform.position);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, m_GetPatrolPointsDistance);
    }
}
