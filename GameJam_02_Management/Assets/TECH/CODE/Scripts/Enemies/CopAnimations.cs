﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopAnimations : MonoBehaviour
{

    [SerializeField] private Animator m_CopAnimator = null;



    private KarenAnimations karenAnimations;
    private void Start()
    {
        karenAnimations = FindObjectOfType<KarenAnimations>();
    }


    public  void PlayArrestedAnimation()
    {
        m_CopAnimator.Play("COP_Arrested");
        //StartCoroutine(IEPlayArrestedAnimation());
    }
    private IEnumerator IEPlayArrestedAnimation()
    {
        yield return new WaitForSeconds(karenAnimations.m_TimeBeforeDiyngFromTupperware);
        m_CopAnimator.Play("COP_Arrested");
    }
}
