﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectiles : MonoBehaviour
{
    [Header("Valeurs pouvant etre changés")]
    [SerializeField] private float m_StunDuration = 0.5f;
    [SerializeField] private float m_MovementSpeed = 10.0f;
    [SerializeField] private float m_DegresRotationPerSecond = 360.0f;
    [SerializeField] private int m_NumberOfTupperDamage = 2;
    [Header("Valeur figees")]
    [SerializeField] private GameObject m_HitZone = null;
    [SerializeField] private LayerMask m_LayerMask;
    [SerializeField] private float m_hitZoneRadius = 0.25f;
    [SerializeField] private float m_RadiusZone = 1.5f;
    [SerializeField] private AudioSource m_HitGroundSound = null;
    
    public Vector3 EndPosition { get; set; }
    public bool IsSpawnedLeft { get; set; }
    private MeshRenderer hitZone;
    private float _rotationSpeed = 0.0f;
    private float _maxDistance = 0.0f;
    private bool _hasReachedPosition = false;
    private float _dissapearZoneSpeed = .5f;
    private float _zoneShowingTime = .15f;
    private bool _travelDone = false;
    private void Start()
    {
        if (IsSpawnedLeft)
        {
            _rotationSpeed = -m_DegresRotationPerSecond;
        }
        else
        {
            _rotationSpeed = m_DegresRotationPerSecond;
        }
        _maxDistance = (EndPosition - transform.position).magnitude;
    }
    private void Update()
    {
        if (_travelDone || !GameManager.s_Instance.isInGame()) return;
        if (hasReachedPosition())
        {
            if (!_hasReachedPosition)
            {
                StartCoroutine(IEHitZoneDissapear());
                _travelDone = true;
                if (m_HitGroundSound != null)
                {
                    m_HitGroundSound.Play();
                }

                _hasReachedPosition = true;
            }
            return;
        }
        else
        {
            Rotate();
            TravelToEndPosition();
            RadiusGrow();
        }
    }
    public void SpawnHitZone()
    {
        //Called By the Spawner
        Vector3 newEndPos = EndPosition;
        newEndPos.y += 0.1f;
        GameObject hitZoneObj = Instantiate(m_HitZone, EndPosition, Quaternion.identity);
        hitZone = hitZoneObj.GetComponent<MeshRenderer>();
        hitZone.material.SetFloat("_Radius", 0.0f);
        hitZone.material.SetFloat("_Opacity", 0.5f);
        hitZone.material.SetFloat("_DistanceNormalized", 0.0f);
    }


    private void TravelToEndPosition()
    {
        transform.position = Vector3.MoveTowards(transform.position, EndPosition, m_MovementSpeed * Time.deltaTime);
    }

    private bool hasReachedPosition()
    {
        return (transform.position - EndPosition).magnitude < 0.1f;
    }
    private void Rotate()
    {
        transform.Rotate(transform.right, _rotationSpeed * Time.deltaTime);
    }

    private void RadiusGrow()
    {
        float _currentDistance = (EndPosition - transform.position).magnitude;
        if (_currentDistance == 0.0 ||hitZone ==null)
        {
            return;
        }
        float lerpValue = _currentDistance / _maxDistance;
        float radius = Mathf.Lerp(0.0f,m_hitZoneRadius,1.0f-lerpValue);
        hitZone.material.SetFloat("_Radius", radius);
        hitZone.material.SetFloat("_DistanceNormalized",1.0f- lerpValue);
    }

    private IEnumerator IEHitZoneDissapear()
    {
        hitZone.material.SetFloat("_Opacity", 1.0f);
        GetKarenInZone();
        yield return new WaitForSeconds(_zoneShowingTime);
        float time = 0.0f;
        float increment = 1.0f / _dissapearZoneSpeed;
        while(time < 1.0f)
        {
      
            time += Time.deltaTime * increment;
            time = time > 1.0f ? 1.0f : time;

            float radius = Mathf.Lerp(m_hitZoneRadius, 0.0f, time);
            hitZone.material.SetFloat("_Radius", radius);
            hitZone.material.SetFloat("_Opacity", 1.0f-time);

            yield return null;
        }

        StartCoroutine(IEGoInTheGround());
    }

    public void GetKarenInZone()
    {
        if (InputListener.s_Instance.IsStunned || GameManager.s_Instance.IsDead ||GameManager.s_Instance.IsGameFinished) return;
        Collider[] karens = Physics.OverlapSphere(transform.position, m_RadiusZone, m_LayerMask);
        if (karens.Length > 0)
        {
            KarenSystems systems = karens[0].GetComponent<KarenSystems>();
            if (systems == null) return;

            PlaySoundsController.s_Instance.PlayKarenGetHit();
            systems.Etourdissement(m_StunDuration, m_NumberOfTupperDamage);
        }
    }


    public IEnumerator IEGoInTheGround()
    {
        yield return new WaitForSeconds(2.0f);
        float time = 0.0f;
        while (time < 1.0f)
        {
            time += Time.deltaTime;
            transform.position += Vector3.down * Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, m_RadiusZone);
    }
}
