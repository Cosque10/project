﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TupperWareController : MonoBehaviour
{
    [SerializeField] private Rigidbody m_Rigidbody = null;
    [SerializeField] private MeshCollider m_Collider = null;
    [SerializeField] private float m_ImpulseForce = 5.0f;
    private IEnumerator Start()
    {
        SpawnImpulse();
        yield return new WaitForSeconds(3.0f);
        m_Rigidbody.useGravity = false;
        m_Collider.enabled = false;
        float time = 0.0f;
        while (time < 1.0f)
        {
            time += Time.deltaTime;
            transform.position += Vector3.down * Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject);
    }

    public void SpawnImpulse()
    {
        float xdirection = Random.Range(-0.3f, 0.3f);
        float ydirection = Random.Range(0.5f, 1.0f);
        float zdirection = Random.Range(-0.3f, 0.3f);

        Vector3 finalDirection = new Vector3(xdirection, ydirection, zdirection).normalized;

        m_Rigidbody.AddForce(finalDirection * m_ImpulseForce, ForceMode.Impulse);
    }

}
