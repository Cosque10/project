﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class FinishController : MonoBehaviour
{
    [SerializeField] private KarenSystems m_KarenSystems = null;
    [SerializeField] private Animator[] m_IdleKarensAnimators = null;
    [SerializeField] private HudController m_HudController = null;
    [SerializeField] private CanvasGroup m_FinishGameCanvasGroup = null;
    [SerializeField] private TextMeshProUGUI m_TimerText = null;
    [SerializeField] private TextMeshProUGUI m_TupperWaresText = null;
    [SerializeField] private TextMeshProUGUI m_ScoreText = null;
    [SerializeField] private AudioSource m_FinishSound = null;

    int tupperWaresRemaining = 0;
    float timeRemaining = 0.0f;

    private void OnEnable()
    {
        m_FinishGameCanvasGroup.alpha = 0.0f;
    }
    private void OnTriggerEnter(Collider other)
    {
        GameManager.s_Instance.IsGameFinished = true;
        ShowScoreHud();
    }


    public void ShowScoreHud()
    {
        StartCoroutine(IEShowScoreHud());
    }

    public void SideKarenDances1()
    {
        float speed = Random.Range(0.5f, 1.2f);
        m_IdleKarensAnimators[0].Play("Karen_Dance");
        m_IdleKarensAnimators[0].speed = speed;
    }
    public void SideKarenDances2()
    {
        float speed = Random.Range(0.5f, 1.2f);
        m_IdleKarensAnimators[1].Play("Karen_Dance");
        m_IdleKarensAnimators[1].speed = speed;
    }

    public void SideKarenDances3()
    {
        float speed = Random.Range(0.5f, 1.2f);
        m_IdleKarensAnimators[2].Play("Karen_Dance");
        m_IdleKarensAnimators[2].speed = speed;
    }


    public IEnumerator IEShowScoreHud()
    {
        if (m_FinishSound != null)
        {
            m_FinishSound.Play();
        }

        Invoke("SideKarenDances1", 0.25f);
        Invoke("SideKarenDances2", 0.1f);
        Invoke("SideKarenDances3", .5f);
        m_KarenSystems.m_KarenAnimations.PlayDanceAnimations();
        timeRemaining = m_HudController._maxTimer;
        tupperWaresRemaining = m_KarenSystems.NumberOfTupperWares;
        m_HudController.HideHud();
        float time = 0.0f;
        while(time < 1.0f)
        {
            time += Time.deltaTime;
            time = Mathf.Min(time, 1.0f);
            m_FinishGameCanvasGroup.alpha = time;
            yield return null;
        }
        yield return new WaitForSeconds(.5f);
        int minutes = Mathf.FloorToInt(timeRemaining / 60);
        string minutesString = minutes < 10 ? $"0{minutes}" : $"{minutes}";

        int secondes = Mathf.RoundToInt(timeRemaining % 60);
        if(secondes == 60)
        {
            secondes = 59;
        }
        string secondesString = secondes < 10 ? $"0{secondes}" : $"{secondes}";
        m_TimerText.text = $"{minutesString}:{secondesString}";
        m_TimerText.gameObject.SetActive(true);
        yield return new WaitForSeconds(.5f);
        m_TupperWaresText.text = $"{tupperWaresRemaining}";
        m_TupperWaresText.gameObject.SetActive(true);
        yield return new WaitForSeconds(.5f);

        m_ScoreText.text = $"{Mathf.RoundToInt(timeRemaining * tupperWaresRemaining)}";
        m_ScoreText.gameObject.SetActive(true);
    }

    public void GoToMainMenu()
    {
        SceneChangeController.s_Instance.RestartScene();
    }

}
