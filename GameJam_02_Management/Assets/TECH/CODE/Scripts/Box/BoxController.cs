﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    [SerializeField] private BoxCollider m_CollisionBox = null;
    [SerializeField] private GameObject m_ButtonInteraction = null;
    [SerializeField] private Animator m_BoxAnimator = null;
    [SerializeField] private BoxCollider m_ColliderTrigger = null;
    [SerializeField] private MeshRenderer m_MeshRend = null;
    [SerializeField] private float m_TimeToDestroy = 0.5f;
    [SerializeField] public int m_Index = 0;

    private bool _canInteract = false;
    private bool _hasInteracted =false;
    private KarenSystems _karenSystems;
    private void OnEnable()
    {
        _karenSystems = FindObjectOfType<KarenSystems>();
    }
    private void OnTriggerEnter(Collider other)
    {
        m_ButtonInteraction.SetActive(true);
        _canInteract = true;
    }


    private void OnTriggerExit(Collider other)
    {
        m_ButtonInteraction.SetActive(false);
        _canInteract = false;
    }

    private void Update()
    {

        TimerFinished();
        if (InputListener.s_Instance.InteractionButton)
        {
            if (!_hasInteracted)
            {
                if (_canInteract)
                {
                    _hasInteracted = true;
                    Interact();
                }
            }
            else
            {
                GettingOut();
            }
           
        }

        if (_hasInteracted)
        {
            m_ButtonInteraction.SetActive(true);
        }

    }

    public void GettingOut()
    {
        m_BoxAnimator.Play("Box_Out");
        _karenSystems.m_KarenAnimations.PlayKarenOutOfBox();
        StartCoroutine(IEBoxDestroying());
        m_ColliderTrigger.enabled = false;
        _hasInteracted = false;
        m_ButtonInteraction.SetActive(false);
    }

    public void TimerFinished()
    {
        if(_hasInteracted && GameManager.s_Instance.IsDead)
        {
            StartCoroutine(IEBoxDestroying());
        }
    }

    public void Interact()
    {
        Vector3 KarenDirection = (transform.position - _karenSystems.transform.position).normalized;
        float distance = (transform.position - _karenSystems.transform.position).magnitude;
        if(distance < 1.0f)
        {
            distance = 1.0f - distance;
            transform.position += KarenDirection * distance;
        }
        else
        {
            distance -= 1.0f;
            transform.position -= KarenDirection * distance;
        }
        transform.rotation = Quaternion.LookRotation(KarenDirection);
        _karenSystems.transform.rotation = Quaternion.LookRotation(KarenDirection);
        InputListener.s_Instance.IsInBox = true;
        m_BoxAnimator.SetTrigger("TriggerAnimation");
        _karenSystems.gameObject.layer = 11;
        _karenSystems.m_KarenAnimations.PlayKarenBoxHide();
        m_CollisionBox.enabled = false;
    }


    public IEnumerator IEBoxDestroying()
    {
        float time = 0.0f;
        float increment = 1.0f / m_TimeToDestroy;
        while(time < 1.0f)
        {
            m_MeshRend.material.SetFloat("_Subs", time * 5.0f);
            time += Time.deltaTime * increment;
            time = time > 1.0f ? 1.0f : time;
            yield return null;
        }
        Destroy(gameObject);
    }
}
