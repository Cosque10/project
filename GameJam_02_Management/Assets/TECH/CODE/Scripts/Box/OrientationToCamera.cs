﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationToCamera : MonoBehaviour
{

    void Update()
    {
        transform.LookAt(Camera.main.transform);
    }
}
