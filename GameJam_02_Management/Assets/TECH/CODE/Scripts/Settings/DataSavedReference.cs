﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData
{
    public string PositionX;
    public string PositionY;
    public string PositionZ;

    public string RotationX;
    public string RotationY;
    public string RotationZ;
    public Vector3 RefPosition;
    public Vector3 RefRotation;

    public int EnemyID;
    public int CurrentPatrolPoint;
    public List<int> PatrolPoints;

    public EnemyData(EnemyAI enemyAI)
    {
        PositionX = $"PositionX{enemyAI.m_Index}";
        PositionY = $"PositionY{enemyAI.m_Index}";
        PositionZ = $"PositionZ{enemyAI.m_Index}";

        RefPosition = enemyAI.transform.position;
        RefRotation = enemyAI.transform.rotation.eulerAngles;

        RotationX = $"RotationX{enemyAI.m_Index}";
        RotationY = $"RotationY{enemyAI.m_Index}";
        RotationZ = $"RotationZ{enemyAI.m_Index}";

        CurrentPatrolPoint = enemyAI._currentPatrolPoint.m_Index;
        PatrolPoints = new List<int>();

        foreach (PatrolPoint pointP in enemyAI._patrolPoints)
        {
            PatrolPoints.Add(pointP.m_Index);
        }
        EnemyID = enemyAI.m_Index;
    }

}

public class BoxesData
{
    public string BoxIndex;
    public int boxInt;

    public BoxesData(BoxController box)
    {
        BoxIndex = $"BoxIndex + {box.m_Index}";
        boxInt = box.m_Index;       
    }
}
public class DataSavedReference : MonoBehaviour
{
    public static DataSavedReference s_Instance { get; private set; }

    // Player
    public string PlayerPositionX { get { return "PlayerPositionX"; } }
    public string PlayerPositionY { get { return "PlayerPositionY"; } }
    public string PlayerPositionZ { get { return "PlayerPositionZ"; } }

    // Camera
    public string CameraPositionX { get { return "CameraPositionX"; } }
    public string CameraPositionY { get { return "CameraPositionY"; } }
    public string CameraPositionZ { get { return "CameraPositionZ"; } }

    public string CameraRotationX { get { return "CameraRotationX"; } }
    public string CameraRotationY { get { return "CameraRotationY"; } }
    public string CameraRotationZ { get { return "CameraRotationZ"; } }


    // Ennemies
    public List<EnemyData> EnemyDataList;
    public List<BoxesData> BoxesDataList;

    // Tupperwares
    public string NumberOfTupperWares { get { return "NumberOfTupperWares"; } }

    public string Timer { get { return "Timer"; } }

    public string Volume { get { return "Volume"; } }

    public string Gama { get { return "Gama"; } }

    public string Brigthness { get { return "Brightness"; } }

     public bool isGameSaved { get; set; }
    private void Awake()
    {
        if (s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddEnemyData(EnemyAI[] enemies)
    {
        EnemyDataList = new List<EnemyData>();
        foreach (EnemyAI enemy in enemies)
        {
            EnemyData data = new EnemyData(enemy);
            EnemyDataList.Add(data);
        }
    }

    public void AddBoxesData(BoxController[] controllers)
    {
        BoxesDataList = new List<BoxesData>();
        foreach (BoxController boxControl in controllers)
        {
            BoxesData data = new BoxesData(boxControl);
            BoxesDataList.Add(data);
        }
    }
}
