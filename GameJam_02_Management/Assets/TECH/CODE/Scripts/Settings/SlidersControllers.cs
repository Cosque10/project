﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
public class SlidersControllers : MonoBehaviour
{
    [SerializeField] private PostProcessVolume m_PPVolume = null;
    [Header("Volume")]
    [SerializeField] private AudioMixer m_MasterVolume = null;
    [SerializeField] private Slider m_AudioSlider = null;
    [Header("Luminosity")]
    [SerializeField] private Slider m_GamaSlider = null;
    [SerializeField] private Slider m_BrightnessSlider = null;


    public void SetVolume()
    {
        m_MasterVolume.SetFloat("allVolumes", m_AudioSlider.value);
    }

    public float GetVolume { get { return m_AudioSlider.value; } }
    public float GetGama { get { return m_GamaSlider.value; } }

    public float GetBrightness { get { return m_BrightnessSlider.value; } }

    public void SetVolumeGamaAndBrightness(float volume,float gama,float brightness)
    {
        m_AudioSlider.value = volume;
        m_BrightnessSlider.value = brightness;
        m_GamaSlider.value = gama;

        m_MasterVolume.SetFloat("allVolumes", volume);

        m_PPVolume.profile.TryGetSettings(out ColorGrading m_ColorGrading);
        if (m_ColorGrading == null) return;
        m_ColorGrading.gamma.value = new Vector4(1, 1, 1, gama);
        m_ColorGrading.postExposure.value = brightness;
    }


    public void SetGamma()
    {
        m_PPVolume.profile.TryGetSettings(out ColorGrading m_ColorGrading);
        if (m_ColorGrading == null) return;
        m_ColorGrading.gamma.value = new Vector4(1, 1, 1, m_GamaSlider.value);
    }

    public void SetBrigthness()
    {
        m_PPVolume.profile.TryGetSettings(out ColorGrading m_ColorGrading);
        if (m_ColorGrading == null) return;
        m_ColorGrading.postExposure.value = m_BrightnessSlider.value;
    }
}
