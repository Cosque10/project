﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavingSystem : MonoBehaviour
{

    private Transform _playerTransform;
    private Transform _cameraTransform;
    private KarenSystems _karenSystem;
    private BoxController _boxController;
    private HudController _hudController;
    private SlidersControllers _sliderController;
    private void OnEnable()
    {
        GetReferences();
        SceneChangeController.s_Instance.restartScene += GetReferences;
        SceneChangeController.s_Instance.loadGame += GetReferences;
    }

    public void GetReferences()
    {
        _playerTransform = FindObjectOfType<CharacterMovement>().transform;
        _cameraTransform = FindObjectOfType<CameraMovement>().transform;
        _karenSystem = FindObjectOfType<KarenSystems>();
        _boxController = FindObjectOfType<BoxController>();
        _hudController = FindObjectOfType<HudController>();
        _sliderController = FindObjectOfType<SlidersControllers>();
    }
    public void Save()
    {
        // Player Position 
        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.PlayerPositionX, _playerTransform.position.x);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.PlayerPositionY, _playerTransform.position.y);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.PlayerPositionZ, _playerTransform.position.z);

        // Camera Position
        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraPositionX, _cameraTransform.position.x);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraPositionY, _cameraTransform.position.y);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraPositionZ, _cameraTransform.position.z);

        Vector3 rotation = _cameraTransform.rotation.eulerAngles;
        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraRotationX, rotation.x);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraRotationY, rotation.y);

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.CameraRotationZ, rotation.z);

        EnemyAI[] enemiesAI = FindObjectsOfType<EnemyAI>();
        DataSavedReference.s_Instance.AddEnemyData(enemiesAI);

        foreach (EnemyData enemy in DataSavedReference.s_Instance.EnemyDataList)
        {
            // Position Set
            PlayerPrefs.SetFloat(enemy.PositionX, enemy.RefPosition.x);
            PlayerPrefs.SetFloat(enemy.PositionY, enemy.RefPosition.y);
            PlayerPrefs.SetFloat(enemy.PositionZ, enemy.RefPosition.z);
            // Rotation Set
            PlayerPrefs.SetFloat(enemy.RotationX, enemy.RefRotation.x);
            PlayerPrefs.SetFloat(enemy.RotationY, enemy.RefRotation.y);
            PlayerPrefs.SetFloat(enemy.RotationZ, enemy.RefRotation.z);
        }

        // Tupper Wares
        PlayerPrefs.SetInt(DataSavedReference.s_Instance.NumberOfTupperWares, _karenSystem.NumberOfTupperWares);

        BoxController[] boxes = FindObjectsOfType<BoxController>();
        DataSavedReference.s_Instance.AddBoxesData(boxes);

        foreach (BoxesData boxController in DataSavedReference.s_Instance.BoxesDataList)
        {
            PlayerPrefs.SetFloat(boxController.BoxIndex, boxController.boxInt);
        }

        PlayerPrefs.SetFloat(DataSavedReference.s_Instance.Timer, _hudController._maxTimer);

        DataSavedReference.s_Instance.isGameSaved = true;
    }
}
