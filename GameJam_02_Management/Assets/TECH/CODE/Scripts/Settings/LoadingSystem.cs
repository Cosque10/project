﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSystem : MonoBehaviour
{
    [SerializeField] private GameObject m_NoGameSavedWarning = null;

    private Transform _playerTransform;
    private Transform _cameraTranform;
    private EnemyAI[] _enemiesScript;
    private KarenSystems _karenSystems;
    private BoxController[] _boxController;
    private HudController _hudController;
    private SlidersControllers _slidersController;
    private void OnEnable()
    {
        SceneChangeController.s_Instance.loadGame += OnGameLoad;
    }
    private void OnDisable()
    {
        SceneChangeController.s_Instance.loadGame -= OnGameLoad;
    }

    public void OnGameLoad()
    {  // Player

        _playerTransform = FindObjectOfType<CharacterMovement>().transform;

        Vector3 _playerPosition = new Vector3(PlayerPrefs.GetFloat(DataSavedReference.s_Instance.PlayerPositionX),
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.PlayerPositionY), 
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.PlayerPositionZ));

        _playerTransform.position = _playerPosition;

        // Camera 
        _cameraTranform = FindObjectOfType<CameraMovement>().transform;
        Vector3 _cameraPosition = new Vector3(PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraPositionX), 
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraPositionY), 
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraPositionZ));

        Vector3 _cameraLocalRotation = new Vector3(PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraRotationX),
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraRotationY),
                                            PlayerPrefs.GetFloat(DataSavedReference.s_Instance.CameraRotationZ));

        _cameraTranform.position = _cameraPosition;
        _cameraTranform.localRotation = Quaternion.Euler(_cameraLocalRotation);

        //Ennemies 
        EnemyAI[] _enemies = FindObjectsOfType<EnemyAI>();
        List<EnemyAI> _enemiesList = new List<EnemyAI>();
        PatrolPoint[] _patrolPoints = FindObjectsOfType<PatrolPoint>();
        
        foreach (EnemyAI enemy in _enemies)
        {
            _enemiesList.Add(enemy);
        }

        while(_enemiesList.Count > 0)
        {
            foreach (EnemyData enemyData in DataSavedReference.s_Instance.EnemyDataList)
            {
                foreach (EnemyAI enemyInList in _enemiesList)
                {
                    if(enemyData.EnemyID == enemyInList.m_Index)
                    {

                        Vector3 position = new Vector3(PlayerPrefs.GetFloat(enemyData.PositionX), 
                                                        PlayerPrefs.GetFloat(enemyData.PositionY),
                                                        PlayerPrefs.GetFloat(enemyData.PositionZ));

                        Vector3 rotation = new Vector3(PlayerPrefs.GetFloat(enemyData.RotationX),
                                                        PlayerPrefs.GetFloat(enemyData.RotationY),
                                                        PlayerPrefs.GetFloat(enemyData.RotationZ));


                        List<PatrolPoint> patrolPointsList = new List<PatrolPoint>();

                        foreach (PatrolPoint point in _patrolPoints)
                        {
                            foreach (int patrolIndex in enemyData.PatrolPoints)
                            {
                                if(patrolIndex == point.m_Index)
                                {
                                    patrolPointsList.Add(point);
                                    if(point.m_Index == enemyData.CurrentPatrolPoint)
                                    {
                                        enemyInList._currentPatrolPoint = point;
                                    }
                                    break;
                                }
                            }
                        }

                        enemyInList.transform.position = position;
                        enemyInList.transform.rotation = Quaternion.Euler(rotation);
     
                        enemyInList._patrolPoints = patrolPointsList;


                        enemyInList.SetNextDestination();
                        _enemiesList.Remove(enemyInList);
                    }
                  
                    break;
                }
            }
        }
        // Number Of TupperWares
        _karenSystems = FindObjectOfType<KarenSystems>();
        _karenSystems.NumberOfTupperWares = PlayerPrefs.GetInt(DataSavedReference.s_Instance.NumberOfTupperWares);
        _karenSystems.ChangeTupperWareText();


        _boxController = FindObjectsOfType<BoxController>();

        List<int> boxesData = new List<int>();
        foreach (BoxesData item in DataSavedReference.s_Instance.BoxesDataList)
        {
            boxesData.Add(item.boxInt);
        }

        foreach (BoxController box in _boxController)
        {
            bool isInList = false;
            foreach (int boxInt in boxesData)
            {
                if(boxInt == box.m_Index)
                {
                    isInList = true;
                    break;
                }
            }

            if (!isInList)
            {
                Destroy(box.gameObject);
            }
        }

        _hudController = FindObjectOfType<HudController>();
        _hudController._maxTimer = PlayerPrefs.GetFloat(DataSavedReference.s_Instance.Timer);
        _hudController.SetTimer(_hudController._maxTimer);

    }
    public void Load()
    {
        if (!DataSavedReference.s_Instance.isGameSaved)
        {
            m_NoGameSavedWarning.SetActive(true);
            return;
        }
        SceneChangeController.s_Instance.LoadGame();    

    }
}
