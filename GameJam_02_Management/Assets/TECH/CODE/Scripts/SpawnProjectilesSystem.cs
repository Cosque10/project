﻿using UnityEngine;

public class SpawnProjectilesSystem : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform m_Target = null;
    [SerializeField] private Projectiles[] m_ProjectilesPrefab = null;
    [SerializeField] private BoxCollider m_HitCollider = null;
    [SerializeField] private BoxCollider[] m_SpawnColliders = null;


    [Header("Timers")]
     public float m_MinimumTimeBeforeSpawn = 1.0f;
     public float m_MaximumTimeBeforeSpawn = 3.0f;


    float currentTime = 0.0f;
    float nextspawnTime = 0.0f;


    private void Update()
    {
        if (!GameManager.s_Instance.isInGame() || GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished) return;
        FollowTarget();
        if (CanSpawn())
        {
            Spawn();
        }
    }


   public void FollowTarget()
    {
        Vector3 nextPosition = new Vector3(m_Target.position.x, transform.position.y, transform.position.z);
        transform.position = nextPosition;
    }


    public bool CanSpawn()
    {
        currentTime += Time.deltaTime;
        return currentTime > nextspawnTime;
    }

    public void Spawn()
    {
        int randomObject = Random.Range(0, m_ProjectilesPrefab.Length);
        int randomLeftRight = Random.Range(0, 2);
        BoxCollider collider = m_SpawnColliders[randomLeftRight];
        Projectiles projectile = m_ProjectilesPrefab[randomObject];
        Vector3 spawnPosition = new Vector3(Random.Range(collider.bounds.min.x, collider.bounds.max.x),
                                            Random.Range(collider.bounds.min.y, collider.bounds.max.y),
                                            Random.Range(collider.bounds.min.z, collider.bounds.max.z));
        
        Vector3 hitPosition = new Vector3(Random.Range(m_HitCollider.bounds.min.x, m_HitCollider.bounds.max.x),
                                            Random.Range(m_HitCollider.bounds.min.y, m_HitCollider.bounds.max.y),
                                            Random.Range(m_HitCollider.bounds.min.z, m_HitCollider.bounds.max.z));

        Projectiles projectileSpawned = Instantiate(projectile, spawnPosition, Quaternion.identity);
        projectileSpawned.EndPosition = hitPosition;
        projectileSpawned.IsSpawnedLeft = randomLeftRight < 1;
        projectileSpawned.SpawnHitZone();
        GetRandomSpawnTime();
    }

    public void GetRandomSpawnTime()
    {
        nextspawnTime = Random.Range(m_MinimumTimeBeforeSpawn, m_MaximumTimeBeforeSpawn);
        currentTime = 0.0f;
    }

}
