﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundsController : MonoBehaviour
{

    public static PlaySoundsController s_Instance { get; private set; }

    [SerializeField] private AudioSource m_KarenGetCaught = null;
    [SerializeField] private AudioSource m_KarenGetHit = null;
    [SerializeField] private AudioSource m_KarenRun = null;
    [SerializeField] private AudioSource m_PlayBoxSound = null;

    private void Awake()
    {
        if(s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlayKarenGetCaught()
    {
        m_KarenGetCaught.Play();
    }

    public void PlayKarenGetHit()
    {
        m_KarenGetHit.Play();
    }

    public void PlayKarenRun()
    {
        m_KarenRun.Play();
    }

    public void StopKarenRun()
    {
        m_KarenRun.Stop();
    }

    public void PlayBoxSound()
    {
        m_PlayBoxSound.Play();
    }
}
