﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KarenRunSounds : MonoBehaviour
{
    [SerializeField] private AudioSource[] m_FootSteps = null;



    public void PlaySound()
    {
        int RandomSound = Random.Range(0, m_FootSteps.Length);
        if(m_FootSteps[RandomSound] != null)
        {
            m_FootSteps[RandomSound].Play();
        }

    }

    public void PlayBoxSound()
    {
        PlaySoundsController.s_Instance.PlayBoxSound();
    }
}
