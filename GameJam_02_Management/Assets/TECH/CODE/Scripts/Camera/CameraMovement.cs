﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform m_TargetTransform = null;
    [SerializeField] private Vector3 m_OffetPosition = Vector3.zero;
    [SerializeField] private float m_CameraMovementSpeed = 10.0f;
    [Header("Before Playing Speeds")]
    [SerializeField] private float m_CameraRotationSpeed = 5.0f;
    [SerializeField] private float m_CameraRotationOffset = 5.0f;
    [SerializeField] private float m_CameraMovementSpeedB = 5.0f;


    private Vector3 _targetPosition = Vector3.zero;
    private Vector3 _targetDirection = Vector3.zero;

    private bool _hasReachedPosition = false;
    private void OnEnable()
    {
       GameManager.s_Instance.gameStarted += GoToFinalPositionAndRotation;
        SceneChangeController.s_Instance.loadGame += OnLoadScene;
    }

    private void OnDisable()
    {
        GameManager.s_Instance.gameStarted -= GoToFinalPositionAndRotation;
        SceneChangeController.s_Instance.loadGame -= OnLoadScene;
    }


    private void Start()
    {
        _targetPosition = m_TargetTransform.position - m_OffetPosition;
    }

    private void Update()
    {
        if (!GameManager.s_Instance.isInGame() || !_hasReachedPosition) return;
        _targetPosition = m_TargetTransform.position - m_OffetPosition;      
    }
    private void FixedUpdate()
    {
        if (!GameManager.s_Instance.isInGame() || !_hasReachedPosition) return;

        Vector3 smoothedPosition = Vector3.Lerp(transform.position, _targetPosition, m_CameraMovementSpeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }

    public void OnLoadScene()
    {
        _hasReachedPosition = true;
    }

    public void GoToFinalPositionAndRotation()
    {
        StartCoroutine(IEGoToFinalPosition());
        StartCoroutine(IEGoToFinalRotation());
    }

    private IEnumerator IEGoToFinalPosition()
    {
        Vector3 _targetPosition = m_TargetTransform.position - m_OffetPosition;
        Vector3 _startPosition = transform.position;
        float time = 0.0f;
        float increment = 1.0f / m_CameraMovementSpeedB;
        while (time < 1.0f)
        {      
            time += Time.deltaTime* increment;
            time = time > 1.0f ? 1.0f : time;
            Vector3 position = Vector3.Lerp(_startPosition, _targetPosition, time);
            transform.position = position;
            yield return null;

        }
        _hasReachedPosition = true;
        GameManager.s_Instance.ChangeState(GameState.inGame);
    }

    private IEnumerator IEGoToFinalRotation()
    {
        Vector3 _position = m_TargetTransform.position - m_OffetPosition;
        Vector3 _targetRotationPosition = m_TargetTransform.position + Vector3.up * m_CameraRotationOffset;
        _targetDirection = (_targetRotationPosition - _position).normalized;
        Quaternion startRotation = transform.rotation;
        Quaternion finalRotation = Quaternion.LookRotation(_targetDirection);
        float time = 0.0f;
        float increment = 1.0f / m_CameraRotationSpeed;
        while (time < 1.0f)
        {
            time += Time.deltaTime * increment;
            time = time > 1.0f ? 1.0f : time;
            
            Quaternion rotation = Quaternion.Lerp(startRotation, finalRotation, time);
            transform.rotation = rotation;
            yield return null;
        }
    }

}