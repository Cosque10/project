﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [Header("Dependenties")]
    [SerializeField] private Rigidbody m_Rigidbody = null;
    [SerializeField] private KarenSystems m_KarenSystems = null;
    [Header("Movement")]
    [SerializeField] private float m_MaxMovementSpeed = 10.0f;
    [SerializeField] private float m_NormalMovementSpeed = 10.0f;
    [SerializeField] private float m_AccelerationMultiplier = 5.0f;
    [SerializeField] private float m_MoveRaycastMaxDistance = 1.0f;
    [SerializeField] private LayerMask m_LayerMask;
    [Header("Rotation")]
    [SerializeField] private float m_RotationSpeed = 10.0f;

    private float _speed = 0.0f;
    private float _timeElapsedSinceMoving = 0.0f;

    private Vector3 _direction = Vector3.zero;
    private Quaternion _nextRotation = Quaternion.identity;


    private Transform _cameraTransform;

    private void Start()
    {
        _direction = Vector3.zero;
        _nextRotation = Quaternion.LookRotation(transform.forward);
        _cameraTransform = Camera.main.transform;
    }
    private void Update()
    {
        if (!GameManager.s_Instance.isInGame() || m_KarenSystems.gotCaught || InputListener.s_Instance.IsInBox) return;
        ComputeNextPositionAndRotation();
    }

    private void FixedUpdate()
    {
        if (!GameManager.s_Instance.isInGame() || m_KarenSystems.gotCaught || InputListener.s_Instance.IsInBox) return;
        m_Rigidbody.MovePosition( transform.position + _direction * _speed * Time.fixedDeltaTime);
        m_Rigidbody.MoveRotation(_nextRotation);
    }



    public void ComputeNextPositionAndRotation()
    {
        
        Vector3 direction = _cameraTransform.forward * InputListener.s_Instance.VerticalAxis + _cameraTransform.right * InputListener.s_Instance.HorizontalAxis;
        direction.y = 0.0f;
        direction = direction.normalized;

        if (direction != Vector3.zero)
        {
            _nextRotation = Quaternion.Slerp(_nextRotation, Quaternion.LookRotation(direction), m_RotationSpeed * Time.deltaTime);
        }

        if (CanMove(direction) || direction == Vector3.zero)
        {
            _timeElapsedSinceMoving = 0.0f;
            _direction = Vector3.zero;
            return;
        }

        _direction = direction;
        _speed = Mathf.Lerp(m_NormalMovementSpeed, m_MaxMovementSpeed, _timeElapsedSinceMoving);

        _timeElapsedSinceMoving += Time.deltaTime * m_AccelerationMultiplier;
        _timeElapsedSinceMoving = _timeElapsedSinceMoving > 1.0f ? 1.0f : _timeElapsedSinceMoving;

    }

    public bool CanMove(Vector3 direction)
    {
        bool canMove = false;
        if(Physics.Raycast(transform.position, direction, out RaycastHit hit, m_MoveRaycastMaxDistance, m_LayerMask))
        {
            canMove = true;
        }
        return canMove;
    }
}
