﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KarenAnimations : MonoBehaviour
{
    [SerializeField] private Animator m_KarenAnimator = null;
    [SerializeField] private Transform m_KarenTransform = null;
    [SerializeField] private float m_MaxRandomTimeIdle = 5.0f;
    [SerializeField] private float m_MinRandomTimeIdle = 2.0f;
    [SerializeField] public float m_TimeBeforeDiyngFromTupperware = 1.0f;
    [SerializeField] private float m_TimeBeforeDiyngFromPolice = 1.0f;

    private float _movementBlend = 0.0f;
    private float _currentRandomTime = 0.0f;
    private bool _canIncremntRandomTime = false;
    private float _randomTime = 0.0f;

    private bool _isInSideAnimations = false;

    private void OnEnable()
    {
        RandomTimeGenerator();
    }
    private void Update()
    {
        _movementBlend = Mathf.Clamp01(InputListener.s_Instance.AnimationBlend);
        m_KarenAnimator.SetFloat("MotionBlend", _movementBlend);
        UpdateRandomTime();
        if(_isInSideAnimations && _movementBlend > 0.0f)
        {
            m_KarenAnimator.Play("Movement");
            _isInSideAnimations = false;
        }
    }


    public void StartIdle()
    {
        _canIncremntRandomTime = true;
    }

    public void ChooseRandomIdle()
    {
        if (!m_KarenAnimator.GetCurrentAnimatorStateInfo(0).IsName("Movement") || _movementBlend > 0.0f) return;
        int randomAnim = Random.Range(0, 2);
        if(randomAnim == 0)
        {
            m_KarenAnimator.Play("Karen_Idle01");
        }
        else
        {
            m_KarenAnimator.Play("Karen_Idle02");
        }
        _isInSideAnimations = true;
    }

    public void PlayDanceAnimations()
    {
        m_KarenAnimator.Play("Karen_Dance");
    }

    public void OutOfBox()
    {
        m_KarenTransform.gameObject.layer = 10;
        InputListener.s_Instance.IsInBox = false;
    }

    public void FinishSideAnimation()
    {
        _isInSideAnimations = false;
    }
    private void UpdateRandomTime()
    {
        if (!_canIncremntRandomTime) return;

        _currentRandomTime += Time.deltaTime;
        if(_currentRandomTime > _randomTime)
        {
            _currentRandomTime = 0.0f;
            _canIncremntRandomTime = false;

            ChooseRandomIdle();
            RandomTimeGenerator();
        }
    }

    private void RandomTimeGenerator()
    {
        _randomTime = Random.Range(m_MinRandomTimeIdle, m_MaxRandomTimeIdle);
    }

    public void PlayKarenStunnedAnimation()
    {
        m_KarenAnimator.SetBool("isStunned", true);
        m_KarenAnimator.Play("Karen_Stunned");
    }

    public void StopStunned()
    {
        m_KarenAnimator.SetBool("isStunned", false);
    }

    public void PlayKarenBoxHide()
    {
        m_KarenAnimator.Play("Karen_Hide_box");
    }

    public void PlayKarenBustedAnimation()
    {
        if (GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished) return;
        StartCoroutine(IEPlayKarenBustedAnimation());
    }

    private IEnumerator IEPlayKarenBustedAnimation()
    {
        GameManager.s_Instance.IsDead = true;
        yield return new WaitForSeconds(m_TimeBeforeDiyngFromTupperware);
        m_KarenAnimator.Play("Karen_Arrested");
    }

    public void PlayKarenNoTupperwareLeft()
    {
        if (GameManager.s_Instance.IsDead ||GameManager.s_Instance.IsGameFinished) return;
        StartCoroutine(IEPlayKarenNoTupperwareLeft());
    }
    private IEnumerator IEPlayKarenNoTupperwareLeft()
    {
        
        GameManager.s_Instance.IsDead = true;
        yield return new WaitForSeconds(m_TimeBeforeDiyngFromTupperware);
        m_KarenAnimator.Play("Karen_Lose");
    }


    public void PlayNoTimeLeft()
    {
        if (GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished) return;
        StartCoroutine(IEPlayNoTimeLeft());
    }

    private IEnumerator IEPlayNoTimeLeft()
    {
        GameManager.s_Instance.IsDead = true;
        yield return new WaitForSeconds(m_TimeBeforeDiyngFromTupperware);
        m_KarenAnimator.Play("Karen_Lose");
    }

    public void PlayKarenOutOfBox()
    {
        m_KarenAnimator.Play("Karen_Box_Out");
    }
}
