﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class KarenSystems : MonoBehaviour
{
    [SerializeField] public CharacterMovement m_KarenMovement = null;
    [SerializeField] public KarenAnimations m_KarenAnimations = null;
    [SerializeField] private ParticleSystem m_StunFX = null;
    [SerializeField] private int m_NumberOfTupperWares = 10;
    [SerializeField] private TextMeshProUGUI m_NumberOfTupperText = null;
    [SerializeField] private TupperWareController[] m_TupperWaresPrefabs = null;

    public bool gotCaught { get; set; }

    public int NumberOfTupperWares { get; set; }
    private void Start()
    {
        gotCaught = false;

    }

    private void OnEnable()
    {
        GameManager.s_Instance.gameStarted += OnStartGame;
    }
    public void OnStartGame()
    {
        NumberOfTupperWares = m_NumberOfTupperWares;
        ChangeTupperWareText();
    }
    public void GotCaught()
    {
        gotCaught = true;
    }

    public void Etourdissement(float etourdissementTime,int TupperwaresToLoose)
    {
        m_StunFX.Play();
        m_KarenAnimations.PlayKarenStunnedAnimation();
        InputListener.s_Instance.IsStunned = true;
        LooseTupper(TupperwaresToLoose);
        Invoke("StopEtourdissement", etourdissementTime);
 
    }
    public void StopEtourdissement()
    {
        m_StunFX.Stop(true,ParticleSystemStopBehavior.StopEmittingAndClear);
        m_KarenAnimations.StopStunned();
        InputListener.s_Instance.IsStunned = false;
    }

    public void ChangeTupperWareText()
    {
        m_NumberOfTupperText.text = $"x {NumberOfTupperWares}";
    }

    public void LooseTupper(int TupperToLoose)
    {
        NumberOfTupperWares -= TupperToLoose;
        NumberOfTupperWares = NumberOfTupperWares < 0 ? 0 : NumberOfTupperWares;
        TupperWaresSpawn(TupperToLoose);
        ChangeTupperWareText();
        if(NumberOfTupperWares <= 0)
        {
            LooseGame();
        }
    }

    public void LooseGame()
    {
        StopEtourdissement();
        m_KarenAnimations.PlayKarenNoTupperwareLeft();
        EndCanvasController.s_Instance.FadeLoseFromTupperWare();
    }

    public void TupperWaresSpawn(int tupperWaresCount)
    {
        for (int i = 0; i < tupperWaresCount; i++)
        {
            int random = Random.Range(0, m_TupperWaresPrefabs.Length);
            TupperWareController tupper = Instantiate(m_TupperWaresPrefabs[random], transform.position, Quaternion.identity);
        }
    }

}
