﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectDifficulty : MonoBehaviour
{
    [SerializeField] private Image m_EasyImage = null;
    [SerializeField] private Image m_InsaneImage = null;
    [SerializeField] private float m_MinEasy = .5f;
    [SerializeField] private float m_MaxEasy = 1.0f;

    [SerializeField] private float m_MinInsane = 0.1f;
    [SerializeField] private float m_MaxInsane = 0.3f;


    private SpawnProjectilesSystem _spawnProjectiles;

    private void OnEnable()
    {
        SceneChangeController.s_Instance.restartScene += OnGameStart;
        SceneChangeController.s_Instance.loadGame += OnGameStart;
        GameManager.s_Instance.gameStarted += OnGameStart;
    }

    private void OnDisable()
    {
        SceneChangeController.s_Instance.restartScene -= OnGameStart;
        SceneChangeController.s_Instance.loadGame -= OnGameStart;
        GameManager.s_Instance.gameStarted -= OnGameStart;
    }

    public void OnGameStart()
    {
        if (m_EasyImage.color == Color.white)
        {
            SelectEasyImage();
        }
        else
        {
            SelectInsaneImage();
        }
    }

    public void SelectEasyImage()
    {
        _spawnProjectiles = FindObjectOfType<SpawnProjectilesSystem>();
        m_EasyImage.color = Color.white;
        m_InsaneImage.color = Color.black;
        _spawnProjectiles.m_MinimumTimeBeforeSpawn = m_MinEasy;
        _spawnProjectiles.m_MaximumTimeBeforeSpawn = m_MaxEasy;
        _spawnProjectiles.GetRandomSpawnTime();
    }

    public void SelectInsaneImage()
    {
        _spawnProjectiles = FindObjectOfType<SpawnProjectilesSystem>();
        m_EasyImage.color = Color.black;
        m_InsaneImage.color = Color.white;
        _spawnProjectiles.m_MinimumTimeBeforeSpawn = m_MinInsane;
        _spawnProjectiles.m_MaximumTimeBeforeSpawn = m_MaxInsane;
        _spawnProjectiles.GetRandomSpawnTime();
    }
}
