﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class HudController : MonoBehaviour
{
    [SerializeField] private RectTransform m_Slider = null;
    [SerializeField] private RectTransform m_TupperWareNumber = null;
    [SerializeField] private Transform m_KarenTransform = null;
    [SerializeField] private KarenAnimations m_KarenAnimations = null;
    [SerializeField] private Transform m_FinalPosition = null;
    [SerializeField] private Slider m_DistanceSlider = null;
    [SerializeField] private float m_TimeToShowHud = 0.5f;
    [SerializeField] private TextMeshProUGUI m_TimerText = null;
    [SerializeField] private float m_NumberOfMinutes = 2.0f;

    private float _sliderOnScreenY = -10.0f;
    private float _sliderOutScreenY = 500.0f;

    private float _tupperWareOnScreenX = 75.0f;
    private float _tupperWareOutScreenX = -250.0f;

    private float _TotalDistance = 0.0f;
    private float _CurrentDistance = 0.0f;

    private bool _hasArriveToFinish = false;
    public float _maxTimer { get; set; }
    private void OnEnable()
    {
       // SceneChangeController.s_Instance.restartScene += GoingOutScreen;
        SceneChangeController.s_Instance.loadGame += GoingInScreen;
        GameManager.s_Instance.gameStarted += GoingInScreen;
        GameManager.s_Instance.gameStarted += OnStartGame;
        GameManager.s_Instance.gamePaused += HideHud;
        GameManager.s_Instance.gameUnpaused += ShowHud;

       
        
    }

    public void OnStartGame()
    {
        _maxTimer = m_NumberOfMinutes * 60;
        SetTimer(_maxTimer);
    }

    private void OnDisable()
    {
        GameManager.s_Instance.gamePaused -= HideHud;
        GameManager.s_Instance.gameUnpaused -= ShowHud;
    }

    private void Start()
    {
        if (m_DistanceSlider == null) return;
        _TotalDistance = (m_KarenTransform.transform.position - m_FinalPosition.position).magnitude;
        _CurrentDistance = (m_KarenTransform.transform.position - m_FinalPosition.position).magnitude;
    }



    private void Update()
    {
        if (!GameManager.s_Instance.isInGame() ||GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished) return;
        if (m_DistanceSlider == null || _hasArriveToFinish) return;
        _CurrentDistance = (m_KarenTransform.transform.position - m_FinalPosition.position).magnitude;
        float normalizedDistance = Mathf.Clamp01(_CurrentDistance / _TotalDistance);
        m_DistanceSlider.value = 1.0f - normalizedDistance;
        if (normalizedDistance <=0.05f) _hasArriveToFinish = true;
        DecrementTimer();
    }
    public void GoingInScreen()
    {
        if (m_Slider == null || m_TupperWareNumber == null) return;
        StartCoroutine(IEGoingInScreen());
    }

    public void GoingOutScreen()
    {
        if (m_Slider == null || m_TupperWareNumber == null) return;
        m_Slider.localPosition = new Vector3(m_Slider.localPosition.x, _sliderOutScreenY, m_Slider.localPosition.z);
        m_TupperWareNumber.localPosition = new Vector3(_tupperWareOutScreenX, m_TupperWareNumber.localPosition.y, m_TupperWareNumber.localPosition.z);
    }
    public IEnumerator IEGoingInScreen()
    {
       
        Vector3 startSliderPosition = m_Slider.localPosition;
        Vector3 finalSliderPosition = new Vector3(m_Slider.localPosition.x,_sliderOnScreenY,m_Slider.localPosition.z);
        Vector3 startTupperPosition = m_TupperWareNumber.localPosition;
        Vector3 finalTupperPosition = m_TupperWareNumber.localPosition + Vector3.right * (Mathf.Abs(_tupperWareOnScreenX) + Mathf.Abs(_tupperWareOutScreenX));
        float time = 0.0f;
        float increment = 1.0f / m_TimeToShowHud;
        while(time < 1.0f)
        {
            time += Time.deltaTime;
            time = time > 1.0f ? 1.0f : time;
            Vector3 goToSliderPosition = Vector3.Lerp(startSliderPosition, finalSliderPosition, time);
            Vector3 goToTupperPosition = Vector3.Lerp(startTupperPosition, finalTupperPosition, time);
            m_Slider.transform.localPosition = goToSliderPosition;
            m_TupperWareNumber.localPosition = goToTupperPosition; 
            yield return null;
        }
    }

    public void HideHud()
    {
        if (m_Slider == null || m_TupperWareNumber == null || m_TimerText==null) return;
        m_Slider.gameObject.SetActive(false);
        m_TupperWareNumber.gameObject.SetActive(false);
        m_TimerText.gameObject.SetActive(false);
    }

    public void ShowHud()
    {
        if (m_Slider == null || m_TupperWareNumber == null || m_TimerText ==null) return;
        m_Slider.gameObject.SetActive(true);
        m_TupperWareNumber.gameObject.SetActive(true);
        m_TimerText.gameObject.SetActive(true);
    }


    public void DecrementTimer()
    {
        _maxTimer -= Time.deltaTime;
        _maxTimer = Mathf.Max(0.0f, _maxTimer);
        SetTimer(_maxTimer);

        if(_maxTimer <= 0.0f)
        {
            Lose();
        }
    }

    public void Lose()
    {
        EndCanvasController.s_Instance.FaseLoseToTimer();
        m_KarenAnimations.PlayNoTimeLeft();
    }

    public void SetTimer(float timer)
    {
        int minutes = Mathf.FloorToInt(timer / 60);
        string minutesString = minutes < 10 ? $"0{minutes}" : $"{minutes}";

        int secondes = Mathf.RoundToInt(timer % 60);
        if (secondes == 60)
        {
            secondes = 59;
        }
        string secondesString = secondes < 10 ? $"0{secondes}" : $"{secondes}";
        m_TimerText.text = $"{minutesString}:{secondesString}";
    }
}
