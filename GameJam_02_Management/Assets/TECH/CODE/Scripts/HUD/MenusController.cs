﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenusController : MonoBehaviour
{
    [SerializeField] private GameObject m_Credits = null;
    [SerializeField] private GameObject m_Controles = null;
    [SerializeField] private GameObject m_Settings = null;
    [SerializeField] private GameObject m_PauseMenu = null;
    [SerializeField] private GameObject m_MainMenu = null;
    [SerializeField] private GameObject m_Difficulty = null;

    [SerializeField] private RectTransform m_MainMenuRect = null;
    [SerializeField] private float m_DistanceToMoveMenu = 750.0f;
    [Header("LoadingSystem")]
    [SerializeField] private LoadingSystem m_LoadGame = null;

    private Vector3 startPosition;
    private void OnEnable()
    {
        startPosition = m_MainMenuRect.localPosition;
        SceneChangeController.s_Instance.loadGame += CloseMainMenu;
        SceneChangeController.s_Instance.loadGame += EndPause;
        SceneChangeController.s_Instance.restartScene += PopMenu;
        //GameManager.s_Instance.gameStarted += PopMenu;
    }
    private void OnDisable()
    {

        SceneChangeController.s_Instance.loadGame -= CloseMainMenu;
        SceneChangeController.s_Instance.loadGame -= EndPause;
        SceneChangeController.s_Instance.restartScene -= PopMenu;
       // GameManager.s_Instance.gameStarted -= PopMenu;
    }

    public void CloseMainMenu()
    {
        m_MainMenu.SetActive(false);
    }
    public void StartGame()
    {       
        StartCoroutine(MenuGoesOut());
    }

    public void LoadGame()
    {
        m_LoadGame.Load();
    }

    public void OpenCredits()
    {
        m_Credits.SetActive(true);
    }

    public void OpenControles()
    {
        m_Controles.SetActive(true);
    }

    public void OpenSettings()
    {
        m_Settings.SetActive(true);
    }


    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartPause()
    {
        m_PauseMenu.SetActive(true);
        GameManager.s_Instance.ChangeState(GameState.inPause);
    }
    public void ClosePauseMenu()
    {
        m_PauseMenu.SetActive(false);
    }
    public void EndPause()
    {
         m_PauseMenu.SetActive(false);
        GameManager.s_Instance.ChangeState(GameState.inGame);
    }

    public void PopMenu()
    {
        m_MainMenuRect.localPosition = startPosition;
        m_MainMenu.gameObject.SetActive(true);
        m_Difficulty.SetActive(true);
    }

    private IEnumerator MenuGoesOut()
    {
        m_Difficulty.SetActive(false);
        GameManager.s_Instance.OnGameStart();
        Vector3 FinalPosition = m_MainMenuRect.localPosition + Vector3.right * m_DistanceToMoveMenu;
        float time = 0.0f;
        while(time < 1.0f)
        {
            time += Time.deltaTime;
            time = time > 1.0f ? 1.0f : time;
            m_MainMenuRect.localPosition = Vector3.Lerp(m_MainMenuRect.localPosition, FinalPosition, time);
            yield return null;
        }
 
    }
}
