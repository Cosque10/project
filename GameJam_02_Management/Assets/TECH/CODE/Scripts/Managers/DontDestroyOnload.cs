﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnload : MonoBehaviour
{
    public  static DontDestroyOnload s_Instance { get; private set; }
    private void Awake()
    {
        if(s_Instance == null)
        {
            s_Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
