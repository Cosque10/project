﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum GameState
{
    inMenu,
    inPause,
    inGame
}
public class GameManager : MonoBehaviour
{
    [SerializeField] private MenusController m_MenusController = null;

    private GameState _currentGameState = GameState.inMenu;

    #region Singleton
    public static GameManager s_Instance { get; private set; }

    private void Awake()
    {
        if(s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion



    public event Action gamePaused;
    public event Action gameUnpaused;
    public event Action gameStarted;

    public bool IsDead { get; set; }
    public bool IsGameFinished { get; set; }
    private void OnEnable()
    {
        SceneChangeController.s_Instance.restartScene += OnRestartGame;
        SceneChangeController.s_Instance.loadGame += OnLoadGame;
    }
    private void Update()
    {
        UpdateStates();
    }

    public void OnLoadGame()
    {
        ChangeState(GameState.inGame);
        InputListener.s_Instance.IsStunned = false;
        InputListener.s_Instance.IsInBox = false;
        GameManager.s_Instance.IsDead = false;
        IsGameFinished = false;
    }
    public void OnGameStart()
    {
        gameStarted?.Invoke();
        GameManager.s_Instance.IsDead = false;
        InputListener.s_Instance.IsStunned = false;
        InputListener.s_Instance.IsInBox = false;
        IsGameFinished = false;
    }

    private void EnterState()
    {
        switch (_currentGameState)
        {
            case GameState.inMenu:
                break;
            case GameState.inPause:
                gamePaused?.Invoke();
                break;
            case GameState.inGame:
                gameUnpaused?.Invoke();
                break;
            default:
                break;
        }
    }

    private void QuitState()
    {
        switch (_currentGameState)
        {
            case GameState.inMenu:
                break;
            case GameState.inPause:
                break;
            case GameState.inGame:
                break;
            default:
                break;
        }
    }

    public void OnRestartGame()
    {
        m_MenusController.ClosePauseMenu();
        InputListener.s_Instance.IsInBox = false;
        InputListener.s_Instance.IsStunned = false;
        GameManager.s_Instance.IsDead = false;
        IsGameFinished = false;
        ChangeState(GameState.inMenu);
    }

    private void UpdateStates()
    {
        switch (_currentGameState)
        {
            case GameState.inMenu:
                UpdateMenu();
                break;
            case GameState.inPause:
                UpdatePause();
                break;
            case GameState.inGame:
                UpdateGame();
                break;
            default:
                break;
        }
    }

    public void ChangeState(GameState state)
    {
        QuitState();
        _currentGameState = state;
        EnterState();
    }

    public void OnMenuLoaded()
    {
        ChangeState(GameState.inMenu);
    }
    private void UpdateMenu()
    {

    }

    private void UpdateGame()
    {
        if (InputListener.s_Instance.PauseButton)
        {
            m_MenusController.StartPause();
        }
    }

    private void UpdatePause()
    {
        if (InputListener.s_Instance.PauseButton)
        {
            m_MenusController.EndPause();
        }
    }

    public bool isInGame()
    {
        return _currentGameState == GameState.inGame;
    }

}
