﻿using System.Collections;
using UnityEngine;

public class EndCanvasController : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_CanvasGroup = null;
    [SerializeField] private GameObject m_LoseToCop = null;
    [SerializeField] private GameObject m_LoseFromTupperWare = null;
    [SerializeField] private GameObject m_LoseToTimer = null;
    public static EndCanvasController s_Instance { get; private set; }

    private void Awake()
    {
        if(s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        SceneChangeController.s_Instance.restartScene += PutCanvasAlphaAt0;
        SceneChangeController.s_Instance.loadGame += PutCanvasAlphaAt0;
    }

    private void PutCanvasAlphaAt0()
    {
        m_CanvasGroup.alpha = 0.0f;
        m_LoseToCop.SetActive(false);
        m_LoseFromTupperWare.SetActive(false);
        m_LoseToTimer.SetActive(false);
    }
    public void FadeLoseTopCop()
    {
        StartCoroutine(IELoseToCop());
    }

    public void FaseLoseToTimer()
    {
        StartCoroutine(IELoseToTimer());
    }
    private IEnumerator IELoseToTimer()
    {
        m_LoseToTimer.SetActive(true);
        yield return new WaitForSeconds(4.0f);
        float time = 0.0f;
        while (time < 1.0f)
        {
            time += Time.deltaTime;
            time = time > 1.0f ? 1.0f : time;
            m_CanvasGroup.alpha = time;
            yield return null;
        }
        yield return new WaitForSeconds(3.0f);
        SceneChangeController.s_Instance.RestartScene();
    }

    private IEnumerator IELoseToCop()
    {
        m_LoseToCop.SetActive(true);
        yield return new WaitForSeconds(4.0f);
        float time = 0.0f;
        while(time < 1.0f)
        { 
            time += Time.deltaTime;
            time = time > 1.0f ? 1.0f : time;
            m_CanvasGroup.alpha = time;
            yield return null;
        }
        yield return new WaitForSeconds(3.0f);
        SceneChangeController.s_Instance.RestartScene();
    }

    public void FadeLoseFromTupperWare()
    {
        StartCoroutine(IELoseFromTupperWare());
    }
    private IEnumerator IELoseFromTupperWare()
    {
        m_LoseFromTupperWare.SetActive(true);
        yield return new WaitForSeconds(4.0f);
        float time = 0.0f;
        while (time < 1.0f)
        {
            time += Time.deltaTime;
            time = time > 1.0f ? 1.0f : time;
            m_CanvasGroup.alpha = time;
            yield return null;
        }
        yield return new WaitForSeconds(3.0f);
        SceneChangeController.s_Instance.RestartScene();
    }
}
