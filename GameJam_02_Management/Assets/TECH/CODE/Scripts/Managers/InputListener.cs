﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputListener : MonoBehaviour
{
    [SerializeField] private KeyCode m_InteractionButton = KeyCode.E;
    [SerializeField] private KeyCode m_PauseButton = KeyCode.Escape;

    public static InputListener s_Instance { get; private set; }


    public float HorizontalAxis { get; private set; }
    public float VerticalAxis { get; private set; }

    public float AnimationBlend { get; private set; }
    public bool InteractionButton { get; private set; }
    public bool PauseButton { get; private set; }

    public bool IsStunned { get; set; }
    public bool IsInBox { get; set; }
    private void Awake()
    {
        #region Singleton
        if (s_Instance == null)
        {
            s_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        #endregion
    }

    private void Update()
    {
        bool canMove = IsStunned || IsInBox || !GameManager.s_Instance.isInGame() || GameManager.s_Instance.IsDead ||GameManager.s_Instance.IsGameFinished;
        bool canUse = IsStunned || !GameManager.s_Instance.isInGame() || GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished;
        PauseButton = GameManager.s_Instance.IsDead || GameManager.s_Instance.IsGameFinished ? false : Input.GetKeyDown(m_PauseButton);
        HorizontalAxis = canMove ? 0.0f :  Input.GetAxisRaw("Horizontal");
        VerticalAxis = canMove ? 0.0f : Input.GetAxisRaw("Vertical");
        AnimationBlend = canMove ? 0.0f : Mathf.Abs(Input.GetAxis("Horizontal")) + Mathf.Abs(Input.GetAxis("Vertical"));
        InteractionButton = canUse ? false : Input.GetKeyDown(m_InteractionButton);
    }
}
