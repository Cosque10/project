// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "S_ObjectsFallingZone"
{
	Properties
	{
		[HDR]_ColorNear("Color Near", Color) = (0,0,0,0)
		_Radius("Radius", Float) = 0
		_Hardness("Hardness", Float) = 0
		_ColorFar("Color Far", Color) = (0,0,0,0)
		_DistanceNormalized("DistanceNormalized", Range( 0 , 1)) = 0
		_Opacity("Opacity", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _ColorFar;
		uniform float4 _ColorNear;
		uniform float _DistanceNormalized;
		uniform float _Opacity;
		uniform float _Radius;
		uniform float _Hardness;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 lerpResult18 = lerp( _ColorFar , _ColorNear , _DistanceNormalized);
			o.Emission = lerpResult18.rgb;
			float2 temp_output_8_0 = ( ( i.uv_texcoord - float2( 0.5,0.5 ) ) / _Radius );
			float dotResult4 = dot( temp_output_8_0 , temp_output_8_0 );
			o.Alpha = ( _Opacity * saturate( ( 1.0 - pow( saturate( dotResult4 ) , _Hardness ) ) ) );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
-1920;0;1920;1019;2646.967;411.631;1.6;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-1965.057,383.251;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;14;-1866.368,536.2734;Inherit;False;Constant;_Vector0;Vector 0;3;0;Create;True;0;0;False;0;False;0.5,0.5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;9;-1512.642,580.628;Inherit;False;Property;_Radius;Radius;1;0;Create;True;0;0;False;0;False;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;6;-1643.763,426.4965;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;8;-1346.421,453.1091;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DotProductOpNode;4;-1186.421,453.1091;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;3;-1042.421,453.1091;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-1043.595,576.1924;Inherit;False;Property;_Hardness;Hardness;2;0;Create;True;0;0;False;0;False;0;50;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;7;-866.4211,453.1091;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;15;-703.7562,452.3802;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;16;-516.7565,451.3802;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;17;-854.3564,-215.2199;Inherit;False;Property;_ColorFar;Color Far;3;0;Create;True;0;0;False;0;False;0,0,0,0;1,0.6745283,0.6745283,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-910.5565,153.8801;Inherit;False;Property;_DistanceNormalized;DistanceNormalized;4;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;-852,-24.5;Inherit;False;Property;_ColorNear;Color Near;0;1;[HDR];Create;True;0;0;False;0;False;0,0,0,0;1,0,0.007261276,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-602.683,307.0042;Inherit;False;Property;_Opacity;Opacity;5;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;18;-540.3564,-126.2199;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-318.6831,313.0042;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;S_ObjectsFallingZone;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;1;False;-1;7;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.1;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;12;0
WireConnection;6;1;14;0
WireConnection;8;0;6;0
WireConnection;8;1;9;0
WireConnection;4;0;8;0
WireConnection;4;1;8;0
WireConnection;3;0;4;0
WireConnection;7;0;3;0
WireConnection;7;1;10;0
WireConnection;15;0;7;0
WireConnection;16;0;15;0
WireConnection;18;0;17;0
WireConnection;18;1;1;0
WireConnection;18;2;19;0
WireConnection;20;0;21;0
WireConnection;20;1;16;0
WireConnection;0;2;18;0
WireConnection;0;9;20;0
ASEEND*/
//CHKSM=C695D6BC4D89DE61A8D9C1854D1E39350FD78BC4