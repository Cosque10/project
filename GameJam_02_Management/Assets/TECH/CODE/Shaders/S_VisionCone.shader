// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "JAM/VisionCone"
{
	Properties
	{
		_pa("pa", Vector) = (0,1,0,0)
		_pb("pb", Vector) = (0,0.5,0,0)
		_ra("ra", Float) = 0.5
		_rb("rb", Float) = 0.3
		_Color("Color", Color) = (0,0,0,0)
		_Opacity("Opacity", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color;
		uniform float2 _pa;
		uniform float2 _pb;
		uniform float _ra;
		uniform float _rb;
		uniform float _Opacity;


		float UnevenCapsule2D68( float2 p, float2 pa, float2 pb, float ra, float rb )
		{
			 p -=pa;
			pb-=pa;
			float h = dot(pb,pb);
			float2 q = float2( dot(p,float2(pb.y,-pb.x)), dot(p,pb) )/h;
			q.x = abs(q.x);
			float b = ra-rb;
			float2 c = float2(sqrt(h-b*b),b);
			 float k = c.x *q.y - c.y*q.x;
			 float m = dot(c,q);
			 float n = dot(q,q);
			if( k < 0.0 ) return sqrt(h*(n            )) - ra;
			    else if( k > c.x ) return sqrt(h*(n+1.0-2.0*q.y)) - rb;
			                       return m                       - ra;
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _Color.rgb;
			float2 p68 = i.uv_texcoord;
			float2 pa68 = _pa;
			float2 pb68 = _pb;
			float ra68 = _ra;
			float rb68 = _rb;
			float localUnevenCapsule2D68 = UnevenCapsule2D68( p68 , pa68 , pb68 , ra68 , rb68 );
			o.Alpha = ( step( localUnevenCapsule2D68 , 0.0 ) * _Opacity );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
212;513.6;761;270;1549.367;-506.7469;1.691957;True;False
Node;AmplifyShaderEditor.Vector2Node;46;-1038.974,406.617;Inherit;False;Property;_pa;pa;0;0;Create;True;0;0;False;0;False;0,1;0.4,0.5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;42;-1090.728,289.7303;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;48;-1048.75,521.2391;Inherit;False;Property;_pb;pb;1;0;Create;True;0;0;False;0;False;0,0.5;0.85,0.5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;50;-1036.657,641.9269;Inherit;False;Property;_ra;ra;2;0;Create;True;0;0;False;0;False;0.5;0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;51;-1037.464,712.9467;Inherit;False;Property;_rb;rb;3;0;Create;True;0;0;False;0;False;0.3;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;68;-627.673,426.1115;Inherit;False; p -=pa@$pb-=pa@$float h = dot(pb,pb)@$float2 q = float2( dot(p,float2(pb.y,-pb.x)), dot(p,pb) )/h@$q.x = abs(q.x)@$float b = ra-rb@$float2 c = float2(sqrt(h-b*b),b)@$$ float k = c.x *q.y - c.y*q.x@$ float m = dot(c,q)@$ float n = dot(q,q)@$$if( k < 0.0 ) return sqrt(h*(n            )) - ra@$    else if( k > c.x ) return sqrt(h*(n+1.0-2.0*q.y)) - rb@$                       return m                       - ra@$;1;False;5;True;p;FLOAT2;0,0;In;;Inherit;False;True;pa;FLOAT2;0,0;In;;Inherit;False;True;pb;FLOAT2;0,0;In;;Inherit;False;True;ra;FLOAT;0;In;;Inherit;False;True;rb;FLOAT;0;In;;Inherit;False;UnevenCapsule2D;True;False;0;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-598.0641,614.1829;Inherit;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;72;-388.0445,484.9473;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-375.1356,612.0853;Inherit;False;Property;_Opacity;Opacity;5;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;73;-385.0965,15.63371;Inherit;False;Property;_Color;Color;4;0;Create;True;0;0;False;0;False;0,0,0,0;0.03239589,0.9811321,0.7753251,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;-215.1905,500.1928;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2.025285,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;JAM/VisionCone;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;68;0;42;0
WireConnection;68;1;46;0
WireConnection;68;2;48;0
WireConnection;68;3;50;0
WireConnection;68;4;51;0
WireConnection;72;0;68;0
WireConnection;72;1;71;0
WireConnection;75;0;72;0
WireConnection;75;1;74;0
WireConnection;0;2;73;0
WireConnection;0;9;75;0
ASEEND*/
//CHKSM=D327AE7684D2247623F84F1CD91E2EE141BB46A5